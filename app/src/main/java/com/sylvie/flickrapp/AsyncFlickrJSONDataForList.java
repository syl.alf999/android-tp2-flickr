package com.sylvie.flickrapp;

import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AsyncFlickrJSONDataForList extends AsyncTask<String, Void, JSONObject> {
    private MyAdapter adapter;

    public AsyncFlickrJSONDataForList(MyAdapter adapter){
        this.adapter = adapter;
    }

    @Override
    protected JSONObject doInBackground(String... strings) {

        JSONObject json = new JSONObject();
        String urlString = strings[0];

        try {
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            //String basicAuth = "Basic " + Base64.encodeToString((login + ":" + password).getBytes(), Base64.NO_WRAP);
            //urlConnection.setRequestProperty ("Authorization", basicAuth);

            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String s = readStream(in);
                json = new JSONObject(s);

            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }

    /**
     * @param jsonObject
     */
    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        Log.i("JFL", jsonObject.toString());

        String imageLink = getImageLinkFromPublic_photos_Flickr_method(jsonObject);
        //String imageLink = getImageLinkFromPhotos_search_Flickr_method(jsonObject);
        adapter.notifyDataSetChanged();
    }


    /**
     * Return the links of an image from Flickr API call
     * @See https://www.flickr.com/services/feeds/docs/photos_public/
     * @param jsonObject
     * @return
     */
    public String getImageLinkFromPublic_photos_Flickr_method(JSONObject jsonObject){
        String url = "";
        try {
            JSONArray items = jsonObject.getJSONArray("items");

            for (int i = 0; i < items.length(); i++) {
                JSONObject item = (JSONObject) items.get(i);
                JSONObject media = item.getJSONObject("media");
                url = media.getString("m");
                Log.i("JFL", "Adding to adapter url : " + url);
                adapter.dd(url);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return url;
    }

    /**
     * Return the ID of an image from a specific Flickr API call
     * The source of an image is formated like this
     * https://live.staticflickr.com/{server}/{id}_{secret}_s.jpg"
     * @See https://www.flickr.com/services/api/flickr.photos.search.html
     * @return
     */
    public String getImageLinkFromPhotos_search_Flickr_method(JSONObject jsonObject){
        String url = "";
        try {
            JSONObject photoSection = jsonObject.getJSONObject("photos");
            JSONArray photosList = photoSection.getJSONArray("photo");

            for (int i = 0; i < photosList.length(); i++) {
                JSONObject item = (JSONObject) photosList.get(i);

                String id = item.getString("id");
                String server = item.getString("server");
                String secret = item.getString("secret");

                url = "https://live.staticflickr.com/"+server+"/"+ id +"_"+ secret + "_m.jpg";
                Log.i("JFL", "Adding to adapter url : " + url);
                adapter.dd(url);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return url;
    }

    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is), 1000);
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }

}