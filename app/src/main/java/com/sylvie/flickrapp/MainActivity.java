package com.sylvie.flickrapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.CancellationToken;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnTokenCanceledListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    Button btnGetImage, btnToListActivity;
    public ImageView image;

    //Google's API for GPS location
    FusedLocationProviderClient fusedLocationProviderClient;
    LocationRequest locationRequest;
    private static final int PERMISSIONS_FINE_LOCATION = 1;
    public double lat = 0, lon = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnGetImage = findViewById(R.id.btnGeImage);
        btnToListActivity = findViewById(R.id.btnToListActivity);
        image = findViewById(R.id.image);

        updateGPS();

        btnGetImage.setOnClickListener(new GetImageOnClickListener());
    }

    /**
     * Initialize the current location of the smartphone
     * Ask for permission if access is not granted
     */
    public void updateGPS(){
        locationRequest = new LocationRequest();
        locationRequest.setInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setFastestInterval(3000);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MainActivity.this);

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){

            Task<Location> loc = fusedLocationProviderClient.getCurrentLocation(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY, new CancellationToken() {
                @Override
                public boolean isCancellationRequested() {
                    return false; //Not implemented
                }

                @NonNull
                @Override
                public CancellationToken onCanceledRequested(@NonNull OnTokenCanceledListener onTokenCanceledListener) {
                    return null; //Not implemented
                }
            });

            loc.addOnSuccessListener(this, location -> {
                lat = location.getLatitude();
                lon = location.getLongitude();
                Log.i("JFL",lat + " " + lon);
            });

        }
        else{
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_FINE_LOCATION);
            }
        }
    }

    /**
     * Handle permission requests
     * In this Flickr app we have requested for internet and location permissions
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case PERMISSIONS_FINE_LOCATION:
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    updateGPS();
                break;
        }
    }


    /**
     * Navigate to ListActivity from MainActivity
     * @param view
     */
    public void btnToListActivity(View view){
        Intent intent = new Intent(MainActivity.this, com.sylvie.flickrapp.ListActivity.class);
        startActivity(intent);
    }


    /**
     * Handle the activity "get a single image"
     */
    public class GetImageOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            String url = "https://api.flickr.com/services/rest/?" + "method=flickr.photos.search" + "&license=4" + "&api_key=4167810fbc47cc7f1da6d151edf224d5" + "&has_geo=1&lat=" + lat + "&lon=" + lon + "&per_page=1&format=json&nojsoncallback=1";

            //String url = "https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json&nojsoncallback=1";
            new AsyncFlickrJSONData(MainActivity.this).execute(url);
        }
    }
}