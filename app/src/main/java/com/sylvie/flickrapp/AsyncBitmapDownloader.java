package com.sylvie.flickrapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class AsyncBitmapDownloader extends AsyncTask<String, Void, Bitmap> {
    private Context context;

    public AsyncBitmapDownloader(Context context) {
        this.context = context;
    }

    @Override
    protected Bitmap doInBackground(String... strings) {

        Bitmap bm = null;
        String urlString = strings[0];

        try {
            URL url = new URL(urlString);
            Log.i("JFL", urlString);

            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            bm = BitmapFactory.decodeStream(in);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return bm;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        MainActivity c = (MainActivity) context;

        c.image.setImageBitmap(bitmap);
        //Toast.makeText(MainActivity.this, "Image loaded", Toast.LENGTH_SHORT).show();
    }
}
