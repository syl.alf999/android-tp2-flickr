package com.sylvie.flickrapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;

import java.util.Vector;

/**
 * Custom adapter
 */
public class MyAdapter extends BaseAdapter {
    private Vector<String> vector;
    private Context context;

    public MyAdapter(Context context, Vector<String> vector) {
        this.context = context;
        this.vector = vector;
    }

    public MyAdapter(Context context) {
        this.context = context;
        this.vector = new Vector<String>();
    }

    @Override
    public int getCount() {
        return vector.size();
    }

    /**
     * Add a received url in the vector
     * @param url
     */
    public void dd(String url){
        vector.add(url);
    }

    @Override
    public String getItem(int position) {
        return vector.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.i("JFL", "TODO");

        //inflateTextView(position, convertView, parent);

        LayoutInflater inflater = LayoutInflater.from(context);
        if(convertView == null){
            convertView = inflater.inflate(R.layout.bitmaplayout, parent, false);
        }

        ImageView ivFlickrResult = (ImageView) convertView.findViewById(R.id.ivFlickrResult);

        // Get a Request
        RequestQueue queue = MySingleton.getInstance(context).getRequestQueue();

        Response.Listener<Bitmap> rep_listener = response -> {
            ivFlickrResult.setImageBitmap(response);
        };

        Response.ErrorListener rep_error_listener = error -> {
            Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            error.printStackTrace();
        };


        ImageRequest imageRequest = new ImageRequest(vector.get(position), rep_listener, 0, 0, ImageView.ScaleType.CENTER_CROP, null, rep_error_listener);
        queue.add(imageRequest);

        return convertView;
    }

    private void inflateImageView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(context);
        if(convertView == null){
            convertView = inflater.inflate(R.layout.bitmaplayout, parent, false);
        }


        ImageView ivFlickrResult = (ImageView) convertView.findViewById(R.id.ivFlickrResult);

        // Get a Request
        RequestQueue queue = MySingleton.getInstance(convertView.getContext()).getRequestQueue();

        Response.Listener<Bitmap> rep_listener = response -> {
            ivFlickrResult.setImageBitmap(response);
        };

        ImageRequest imageRequest = new ImageRequest(vector.get(position), rep_listener, 0, 0, ImageView.ScaleType.CENTER_CROP, null,
                new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        queue.add(imageRequest);
    }

    /**
     *
     * @param position
     * @param convertView
     * @param parent
     */
    private void inflateTextView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(context);
        if(convertView == null){
            convertView = inflater.inflate(R.layout.textviewlayout, parent, false);
        }

        TextView tvFlickrResult = (TextView) convertView.findViewById(R.id.tvFlickrResult);
        tvFlickrResult.setText(vector.get(position));

    }
}