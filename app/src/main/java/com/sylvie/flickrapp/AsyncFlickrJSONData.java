package com.sylvie.flickrapp;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AsyncFlickrJSONData extends AsyncTask<String, Void, JSONObject> {
    private Context context;

    public AsyncFlickrJSONData(Context context) {
        this.context = context;
    }

    /**
     * Call the API link in the background
     * @param strings
     * @return
     */
    @Override
    protected JSONObject doInBackground(String... strings) {
        URL url = null;
        JSONObject json = new JSONObject();

        String urlString = strings[0];
        try {
            url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            //String basicAuth = "Basic " + Base64.encodeToString((login + ":" + password).getBytes(), Base64.NO_WRAP);
            //urlConnection.setRequestProperty ("Authorization", basicAuth);

            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String s = readStream(in);
                json = new JSONObject(s);

            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }


    /**
     * @param jsonObject
     */
    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        Log.i("JFL", jsonObject.toString());

        String imageLink = getImageLinkFromPhotos_search_Flickr_method(jsonObject);
        //String imageLink = getImageLinkFromPublic_photos_Flickr_method(jsonObject);
        new AsyncBitmapDownloader(context).execute(imageLink);
    }


    /**
     * Return the links of an image from Flickr API call
     * @See https://www.flickr.com/services/feeds/docs/photos_public/
     * @param jsonObject
     * @return
     */
    public String getImageLinkFromPublic_photos_Flickr_method(JSONObject jsonObject){
        String imageLink = "";
        try {
            JSONObject firstItem = jsonObject.getJSONArray("items").getJSONObject(0);
            JSONObject media = firstItem.getJSONObject("media");
            imageLink = media.getString("m");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return imageLink;
    }

    /**
     * Return the ID of an image from a specific Flickr API call
     * The source of an image is formated like this
     * https://live.staticflickr.com/{server}/{id}_{secret}_s.jpg"
     * @See https://www.flickr.com/services/api/flickr.photos.search.html
     * @return
     */
    public String getImageLinkFromPhotos_search_Flickr_method(JSONObject jsonObject){
        String imageLink = "";
        try {
            JSONObject photoSection = jsonObject.getJSONObject("photos");
            JSONArray photosList = photoSection.getJSONArray("photo");
            JSONObject firstPhoto = photosList.getJSONObject(0);

            String id = firstPhoto.getString("id");
            String server = firstPhoto.getString("server");
            String secret = firstPhoto.getString("secret");

            imageLink = "https://live.staticflickr.com/"+server+"/"+ id +"_"+ secret + "_m.jpg";

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return imageLink;
    }

    /**
     * Read an inputStream
     * @param is
     * @return
     * @throws IOException
     */
    private String readStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(is), 1000);
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        is.close();
        return sb.toString();
    }

}